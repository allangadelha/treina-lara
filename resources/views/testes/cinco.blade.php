@extends('template')

@section('title')
    
    {{$posts['title']}} | Treinamento Laravel Express
    
@stop
@section('content')
<h2>Publicações</h2>
<p><i>Brasil, {{ date("d/m/Y") }}</i></p>
<hr>

    @include('sideBar')
    <div class="row" style="float: right; width: 65%">
        
    <h3>{{ $posts['title'] }}</h3>
    @foreach($posts['paras'] as $post)
        <p>{{ $post }}</p>
    @endforeach
        <a href="/treinamento-laravel"><i>Página Inicial</i></a></p>
    </div>
    
@stop