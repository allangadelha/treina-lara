@extends('template')

@section('title')
    
    Treinamento Laravel Express
    
@stop
@section('content')
<h2>Publicações</h2>
<p><i>Brasil, {{ date("d/m/Y") }}</i></p>
<hr>

    @include('sideBar')
    <div class="row" style="float: right; width: 65%">
    @foreach($posts as $post)
        <h3>{{ $post['0'] }}</h3>
        <p>{{ $post['1'] }} </p>
        <a href="#"><i>Leia mais...</i></a>
    @endforeach
    </div>
    
@stop