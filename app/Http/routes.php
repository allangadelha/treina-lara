<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

//Route::get("testes", function (){
//    return "Vamos lá, Allan";
//});

//Route::get('testes', 'TesteController@index');


Route::get('/', 'TesteController@index');
Route::get('/um', 'TesteController@um');
Route::get('/dois', 'TesteController@dois');
Route::get('/tres', 'TesteController@tres');
Route::get('/quatro', 'TesteController@quatro');
Route::get('/cinco', 'TesteController@cinco');
Route::get('/seis', 'TesteController@seis');
