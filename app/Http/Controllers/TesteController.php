<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TesteController extends Controller
{
    
    public function index(){
        
        $dados = array(
            '0' => array ('um', 'Post de Boas Vindas'),
            '1' => array ('dois', 'Segunda Postagem'),
            '3' => array ('tres', 'Mais um para vocês'),
            '4' => array ('quatro', 'Novamente de volta'),
            '2' => array ('cinco', 'Sobre minhas opiniões'),
            '5' => array ('seis', 'Mais do mesmo')
        );
        
        $posts = array( 
            '0' => array ('Post de Boas Vindas','As experiências acumuladas demonstram que a percepção das dificuldades nos obriga à análise do processo de comunicação como um todo. Por outro lado, o acompanhamento das preferências de consumo estimula a padronização do fluxo de informações. Percebemos, cada vez mais, que a complexidade dos estudos efetuados exige a precisão e a definição do sistema de participação geral. Acima de tudo, é fundamental ressaltar que a valorização de fatores subjetivos assume importantes posições no estabelecimento das posturas dos órgãos dirigentes com relação às suas atribuições. Do mesmo modo, o fenômeno da Internet acarreta um processo de reformulação e modernização do retorno esperado a longo prazo.'), 
            '1' => array ('Segunda Postagem','A prática cotidiana prova que o entendimento das metas propostas representa uma abertura para a melhoria de todos os recursos funcionais envolvidos. Neste sentido, o surgimento do comércio virtual auxilia a preparação e a composição das novas proposições. O cuidado em identificar pontos críticos no comprometimento entre as equipes oferece uma interessante oportunidade para verificação do investimento em reciclagem técnica. No entanto, não podemos esquecer que a consolidação das estruturas causa impacto indireto na reavaliação dos conhecimentos estratégicos para atingir a excelência. O empenho em analisar a necessidade de renovação processual maximiza as possibilidades por conta do levantamento das variáveis envolvidas.'), 
            '3' => array ('Mais um para vocês','Gostaria de enfatizar que a adoção de políticas descentralizadoras é uma das consequências do sistema de formação de quadros que corresponde às necessidades. Não obstante, a expansão dos mercados mundiais obstaculiza a apreciação da importância dos relacionamentos verticais entre as hierarquias. Pensando mais a longo prazo, a execução dos pontos do programa possibilita uma melhor visão global das condições financeiras e administrativas exigidas.'),
            '4' => array ('Novamente de volta','Nunca é demais lembrar o peso e o significado destes problemas, uma vez que a hegemonia do ambiente político não pode mais se dissociar dos métodos utilizados na avaliação de resultados. Ainda assim, existem dúvidas a respeito de como a mobilidade dos capitais internacionais aponta para a melhoria de alternativas às soluções ortodoxas. Caros amigos, o aumento do diálogo entre os diferentes setores produtivos estende o alcance e a importância das regras de conduta normativas. Assim mesmo, a crescente influência da mídia garante a contribuição de um grupo importante na determinação da gestão inovadora da qual fazemos parte.'), 
            '2' => array ('Sobre minhas opiniões','A nível organizacional, o início da atividade geral de formação de atitudes agrega valor ao estabelecimento dos modos de operação convencionais. A certificação de metodologias que nos auxiliam a lidar com a competitividade nas transações comerciais prepara-nos para enfrentar situações atípicas decorrentes das formas de ação. É importante questionar o quanto a constante divulgação das informações promove a alavancagem dos procedimentos normalmente adotados. No mundo atual, a determinação clara de objetivos talvez venha a ressaltar a relatividade dos índices pretendidos.'),
            '5' => array ('Mais do mesmo','Por conseguinte, o novo modelo estrutural aqui preconizado ainda não demonstrou convincentemente que vai participar na mudança dos níveis de motivação departamental. Desta maneira, a consulta aos diversos militantes cumpre um papel essencial na formulação do remanejamento dos quadros funcionais. O que temos que ter sempre em mente é que a revolução dos costumes desafia a capacidade de equalização das direções preferenciais no sentido do progresso. É claro que o desafiador cenário globalizado pode nos levar a considerar a reestruturação do impacto na agilidade decisória.') 
        );
       
        return view('testes.index', ['posts' => $posts, 'dados' => $dados]);
    }
    public function um(){
        
        $dados = array(
            '0' => array ('um', 'Post de Boas Vindas'),
            '1' => array ('dois', 'Segunda Postagem'),
            '3' => array ('tres', 'Mais um para vocês'),
            '4' => array ('quatro', 'Novamente de volta'),
            '2' => array ('cinco', 'Sobre minhas opiniões'),
            '5' => array ('seis', 'Mais do mesmo')
        );
        
        $posts = array( 
            'title' => 'Post de Boas Vindas',
            'paras' => array (
                'As experiências acumuladas demonstram que a percepção das dificuldades nos obriga à análise do processo de comunicação como um todo. Por outro lado, o acompanhamento das preferências de consumo estimula a padronização do fluxo de informações. Percebemos, cada vez mais, que a complexidade dos estudos efetuados exige a precisão e a definição do sistema de participação geral. Acima de tudo, é fundamental ressaltar que a valorização de fatores subjetivos assume importantes posições no estabelecimento das posturas dos órgãos dirigentes com relação às suas atribuições. Do mesmo modo, o fenômeno da Internet acarreta um processo de reformulação e modernização do retorno esperado a longo prazo.', 
                'O empenho em analisar a expansão dos mercados mundiais afeta positivamente a correta previsão dos níveis de motivação departamental. Por outro lado, o surgimento do comércio virtual obstaculiza a apreciação da importância do impacto na agilidade decisória. Assim mesmo, o novo modelo estrutural aqui preconizado exige a precisão e a definição dos métodos utilizados na avaliação de resultados. Gostaria de enfatizar que a estrutura atual da organização auxilia a preparação e a composição das posturas dos órgãos dirigentes com relação às suas atribuições.',
                'Do mesmo modo, a competitividade nas transações comerciais representa uma abertura para a melhoria das novas proposições. A prática cotidiana prova que o desenvolvimento contínuo de distintas formas de atuação assume importantes posições no estabelecimento do remanejamento dos quadros funcionais. Pensando mais a longo prazo, a constante divulgação das informações deve passar por modificações independentemente dos conhecimentos estratégicos para atingir a excelência. A certificação de metodologias que nos auxiliam a lidar com a consolidação das estruturas é uma das consequências dos relacionamentos verticais entre as hierarquias.'
                )
        );
       
        return view('testes.um', ['posts' => $posts, 'dados' => $dados]);
    }
    public function dois(){
        
        $dados = array(
            '0' => array ('um', 'Post de Boas Vindas'),
            '1' => array ('dois', 'Segunda Postagem'),
            '3' => array ('tres', 'Mais um para vocês'),
            '4' => array ('quatro', 'Novamente de volta'),
            '2' => array ('cinco', 'Sobre minhas opiniões'),
            '5' => array ('seis', 'Mais do mesmo')
        );        
               
        $posts = array( 
            'title' => 'Segunda Postagem',
            'paras' => array (
                'A prática cotidiana prova que o entendimento das metas propostas representa uma abertura para a melhoria de todos os recursos funcionais envolvidos. Neste sentido, o surgimento do comércio virtual auxilia a preparação e a composição das novas proposições. O cuidado em identificar pontos críticos no comprometimento entre as equipes oferece uma interessante oportunidade para verificação do investimento em reciclagem técnica. No entanto, não podemos esquecer que a consolidação das estruturas causa impacto indireto na reavaliação dos conhecimentos estratégicos para atingir a excelência. O empenho em analisar a necessidade de renovação processual maximiza as possibilidades por conta do levantamento das variáveis envolvidas.',
                'É claro que a percepção das dificuldades aponta para a melhoria das direções preferenciais no sentido do progresso. Não obstante, o acompanhamento das preferências de consumo acarreta um processo de reformulação e modernização do sistema de participação geral. Evidentemente, o desafiador cenário globalizado agrega valor ao estabelecimento das regras de conduta normativas. Todas estas questões, devidamente ponderadas, levantam dúvidas sobre se a revolução dos costumes oferece uma interessante oportunidade para verificação das condições inegavelmente apropriadas.',
                'Por conseguinte, a consulta aos diversos militantes prepara-nos para enfrentar situações atípicas decorrentes das formas de ação. O que temos que ter sempre em mente é que a mobilidade dos capitais internacionais talvez venha a ressaltar a relatividade das condições financeiras e administrativas exigidas. O incentivo ao avanço tecnológico, assim como a contínua expansão de nossa atividade ainda não demonstrou convincentemente que vai participar na mudança dos índices pretendidos.',
                'No entanto, não podemos esquecer que o comprometimento entre as equipes maximiza as possibilidades por conta do processo de comunicação como um todo. Todavia, a adoção de políticas descentralizadoras cumpre um papel essencial na formulação da gestão inovadora da qual fazemos parte. A nível organizacional, a determinação clara de objetivos garante a contribuição de um grupo importante na determinação dos modos de operação convencionais. Caros amigos, a crescente influência da mídia promove a alavancagem das diretrizes de desenvolvimento para o futuro. Percebemos, cada vez mais, que o fenômeno da Internet facilita a criação dos procedimentos normalmente adotados.',
                'Podemos já vislumbrar o modo pelo qual o julgamento imparcial das eventualidades pode nos levar a considerar a reestruturação do retorno esperado a longo prazo. É importante questionar o quanto o início da atividade geral de formação de atitudes desafia a capacidade de equalização dos paradigmas corporativos. Neste sentido, o aumento do diálogo entre os diferentes setores produtivos estimula a padronização do levantamento das variáveis envolvidas. Acima de tudo, é fundamental ressaltar que a hegemonia do ambiente político não pode mais se dissociar das diversas correntes de pensamento. As experiências acumuladas demonstram que a execução dos pontos do programa apresenta tendências no sentido de aprovar a manutenção de todos os recursos funcionais envolvidos.'
), 
        );
       
        return view('testes.dois', ['posts' => $posts, 'dados' => $dados]);
    }
    public function tres(){
        
        $dados = array(
            '0' => array ('um', 'Post de Boas Vindas'),
            '1' => array ('dois', 'Segunda Postagem'),
            '3' => array ('tres', 'Mais um para vocês'),
            '4' => array ('quatro', 'Novamente de volta'),
            '2' => array ('cinco', 'Sobre minhas opiniões'),
            '5' => array ('seis', 'Mais do mesmo')
        );        
        
        $posts = array( 
            'title' => 'Mais um para vocês',
            'paras' => array (
                'Gostaria de enfatizar que a adoção de políticas descentralizadoras é uma das consequências do sistema de formação de quadros que corresponde às necessidades. Não obstante, a expansão dos mercados mundiais obstaculiza a apreciação da importância dos relacionamentos verticais entre as hierarquias. Pensando mais a longo prazo, a execução dos pontos do programa possibilita uma melhor visão global das condições financeiras e administrativas exigidas.',
                'Ainda assim, existem dúvidas a respeito de como a necessidade de renovação processual estende o alcance e a importância do fluxo de informações. No mundo atual, a complexidade dos estudos efetuados possibilita uma melhor visão global do orçamento setorial. Desta maneira, o entendimento das metas propostas nos obriga à análise de alternativas às soluções ortodoxas. Nunca é demais lembrar o peso e o significado destes problemas, uma vez que o consenso sobre a necessidade de qualificação causa impacto indireto na reavaliação do investimento em reciclagem técnica. O cuidado em identificar pontos críticos na valorização de fatores subjetivos faz parte de um processo de gerenciamento do sistema de formação de quadros que corresponde às necessidades.',
                'Pensando mais a longo prazo, o início da atividade geral de formação de atitudes facilita a criação dos procedimentos normalmente adotados. Acima de tudo, é fundamental ressaltar que a competitividade nas transações comerciais causa impacto indireto na reavaliação dos índices pretendidos. Todas estas questões, devidamente ponderadas, levantam dúvidas sobre se a crescente influência da mídia acarreta um processo de reformulação e modernização dos métodos utilizados na avaliação de resultados.',
            )
        );
       
        return view('testes.tres', ['posts' => $posts, 'dados' => $dados]);
    }
    public function quatro(){
        
        $dados = array(
            '0' => array ('um', 'Post de Boas Vindas'),
            '1' => array ('dois', 'Segunda Postagem'),
            '3' => array ('tres', 'Mais um para vocês'),
            '4' => array ('quatro', 'Novamente de volta'),
            '2' => array ('cinco', 'Sobre minhas opiniões'),
            '5' => array ('seis', 'Mais do mesmo')
        );               
        
        $posts = array( 
            'title' => 'Novamente de volta',
            'paras' => array (
                'Nunca é demais lembrar o peso e o significado destes problemas, uma vez que a hegemonia do ambiente político não pode mais se dissociar dos métodos utilizados na avaliação de resultados. Ainda assim, existem dúvidas a respeito de como a mobilidade dos capitais internacionais aponta para a melhoria de alternativas às soluções ortodoxas. Caros amigos, o aumento do diálogo entre os diferentes setores produtivos estende o alcance e a importância das regras de conduta normativas. Assim mesmo, a crescente influência da mídia garante a contribuição de um grupo importante na determinação da gestão inovadora da qual fazemos parte.',
                'É claro que o consenso sobre a necessidade de qualificação auxilia a preparação e a composição das regras de conduta normativas. Do mesmo modo, o julgamento imparcial das eventualidades obstaculiza a apreciação da importância das novas proposições. Nunca é demais lembrar o peso e o significado destes problemas, uma vez que a hegemonia do ambiente político assume importantes posições no estabelecimento do remanejamento dos quadros funcionais.',
                'Todavia, a constante divulgação das informações possibilita uma melhor visão global dos modos de operação convencionais. O incentivo ao avanço tecnológico, assim como o acompanhamento das preferências de consumo oferece uma interessante oportunidade para verificação dos relacionamentos verticais entre as hierarquias. No entanto, não podemos esquecer que a contínua expansão de nossa atividade ainda não demonstrou convincentemente que vai participar na mudança dos paradigmas corporativos. Podemos já vislumbrar o modo pelo qual a consolidação das estruturas promove a alavancagem das posturas dos órgãos dirigentes com relação às suas atribuições. Evidentemente, a expansão dos mercados mundiais agrega valor ao estabelecimento do sistema de formação de quadros que corresponde às necessidades.',
                'Percebemos, cada vez mais, que o novo modelo estrutural aqui preconizado nos obriga à análise de alternativas às soluções ortodoxas. Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação garante a contribuição de um grupo importante na determinação do sistema de participação geral. As experiências acumuladas demonstram que a mobilidade dos capitais internacionais aponta para a melhoria da gestão inovadora da qual fazemos parte. O que temos que ter sempre em mente é que a revolução dos costumes talvez venha a ressaltar a relatividade do impacto na agilidade decisória.',
            ), 
        );
       
        return view('testes.quatro', ['posts' => $posts, 'dados' => $dados]);
    }
    public function cinco(){
        
        $dados = array(
            '0' => array ('um', 'Post de Boas Vindas'),
            '1' => array ('dois', 'Segunda Postagem'),
            '3' => array ('tres', 'Mais um para vocês'),
            '4' => array ('quatro', 'Novamente de volta'),
            '2' => array ('cinco', 'Sobre minhas opiniões'),
            '5' => array ('seis', 'Mais do mesmo')
        );    
                
        $posts = array( 
            'title' => 'Sobre minhas opiniões',
            'paras' => array (
                'A nível organizacional, o início da atividade geral de formação de atitudes agrega valor ao estabelecimento dos modos de operação convencionais. A certificação de metodologias que nos auxiliam a lidar com a competitividade nas transações comerciais prepara-nos para enfrentar situações atípicas decorrentes das formas de ação. É importante questionar o quanto a constante divulgação das informações promove a alavancagem dos procedimentos normalmente adotados. No mundo atual, a determinação clara de objetivos talvez venha a ressaltar a relatividade dos índices pretendidos.',
                'Caros amigos, o comprometimento entre as equipes deve passar por modificações independentemente do processo de comunicação como um todo. Não obstante, o aumento do diálogo entre os diferentes setores produtivos cumpre um papel essencial na formulação das direções preferenciais no sentido do progresso. A nível organizacional, o surgimento do comércio virtual prepara-nos para enfrentar situações atípicas decorrentes dos conhecimentos estratégicos para atingir a excelência. Gostaria de enfatizar que a percepção das dificuldades faz parte de um processo de gerenciamento das condições inegavelmente apropriadas. Por conseguinte, o fenômeno da Internet pode nos levar a considerar a reestruturação das diretrizes de desenvolvimento para o futuro.',
                'É importante questionar o quanto a adoção de políticas descentralizadoras apresenta tendências no sentido de aprovar a manutenção do retorno esperado a longo prazo. O empenho em analisar a complexidade dos estudos efetuados maximiza as possibilidades por conta das diversas correntes de pensamento. Neste sentido, o entendimento das metas propostas estimula a padronização do levantamento das variáveis envolvidas.',
            ),
        );
       
        return view('testes.cinco', ['posts' => $posts, 'dados' => $dados]);
    }
    public function seis(){
        
        $dados = array(
            '0' => array ('um', 'Post de Boas Vindas'),
            '1' => array ('dois', 'Segunda Postagem'),
            '3' => array ('tres', 'Mais um para vocês'),
            '4' => array ('quatro', 'Novamente de volta'),
            '2' => array ('cinco', 'Sobre minhas opiniões'),
            '5' => array ('seis', 'Mais do mesmo')
        );        
        
        $posts = array( 
            'title' => 'Mais do mesmo',
            'paras' => array (
                'Por conseguinte, o novo modelo estrutural aqui preconizado ainda não demonstrou convincentemente que vai participar na mudança dos níveis de motivação departamental. Desta maneira, a consulta aos diversos militantes cumpre um papel essencial na formulação do remanejamento dos quadros funcionais. O que temos que ter sempre em mente é que a revolução dos costumes desafia a capacidade de equalização das direções preferenciais no sentido do progresso. É claro que o desafiador cenário globalizado pode nos levar a considerar a reestruturação do impacto na agilidade decisória.',
                'Por outro lado, a consulta aos diversos militantes não pode mais se dissociar do orçamento setorial. A certificação de metodologias que nos auxiliam a lidar com a execução dos pontos do programa representa uma abertura para a melhoria de todos os recursos funcionais envolvidos. Ainda assim, existem dúvidas a respeito de como a necessidade de renovação processual estende o alcance e a importância das formas de ação. No mundo atual, o desafiador cenário globalizado afeta positivamente a correta previsão do fluxo de informações. ',
                'Desta maneira, a estrutura atual da organização é uma das consequências dos níveis de motivação departamental. A prática cotidiana prova que a determinação clara de objetivos desafia a capacidade de equalização do investimento em reciclagem técnica. O cuidado em identificar pontos críticos na valorização de fatores subjetivos exige a precisão e a definição das condições financeiras e administrativas exigidas. Do mesmo modo, o início da atividade geral de formação de atitudes exige a precisão e a definição das diversas correntes de pensamento.',
            ) 
        );
       
        return view('testes.seis', ['posts' => $posts, 'dados' => $dados]);
    }

}
